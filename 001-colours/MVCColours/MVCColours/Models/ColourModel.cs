﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace MVCColours.Models
{
    public class ColourModel
    {

        public string Name { get; set; }
        public string ColourName { get; private set; }
        public string ColourHex { get; private set; }

        public ColourModel(string name)
        {
            Name = name;

            string firstLetter = name[0].ToString();

            SqlConnection sqlConnection = new SqlConnection("Server=tcp:jonatanjaworski.database.windows.net,1433;Database=colours;User ID=Alturil@jonatanjaworski;Password=Elbereth1;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            SqlDataReader reader;

            SqlCommand storedProcCommand = new SqlCommand("get_colour", sqlConnection);
            storedProcCommand.CommandType = CommandType.StoredProcedure;
            storedProcCommand.Parameters.AddWithValue("@Letter", firstLetter);

            sqlConnection.Open();

            reader = storedProcCommand.ExecuteReader();

            while (reader.Read())
            {
                ColourName = reader.GetString(reader.GetOrdinal("colour_name"));
                ColourHex = reader.GetString(reader.GetOrdinal("colour_hex"));
            }

            sqlConnection.Close();
        }                

    }
}
